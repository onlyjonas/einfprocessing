void setup(){

size(200,200);
frameRate(10);
noStroke();
fill(0);
rectMode(CENTER);
}

void draw() {
background(255);
  for (int i=0; i<10; i++) {
    for (int j=0; j<10; j++) {
    fill(i/10.0*255,0,j/10.0*255);
    float groesse = random(1,9);
    rect(10+i*20,10+j*20,groesse,groesse); 
    }
  }
}
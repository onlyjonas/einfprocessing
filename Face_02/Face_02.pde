/* * * Face 02 * * * */
/* Interaktion * * * */

/* Variables */
color blue, darkblue, lightblue, orange, red;
PShape wolke;
float wolkenPos;
String[] texts = {
    "Hallo!",
    "Wie geht es Dir?",
    "Super! \nDas freut mich!"
  };
int currentTextID = 0;
boolean showMe = false;
float startTime;

void setup() {

  /* Config */
  // Colors
  blue = color(34, 92, 204);
  darkblue = color(65, 95, 153);
  lightblue = color(0, 221, 255);
  orange = color(255, 135, 85);
  red = color(204, 31, 0);

  // Shapes
  wolke = loadShape("Wolke.svg");

  // Size
  size(800, 600);
  
  // Activate AudioHandler
  setupAudio();
}

void draw() {

  displayBackground();
  displayBlankFace(200, 0);  
  displayEye(530, 200, 50, 25);
  displayEye(450, 200, 30, 15);
  displayMouth(490, 350, 2, 50);

  // Say somethink if volume is high
  if(volume > 80 && showMe == false)  {
    showMe = true;
    startTime = millis();
    currentTextID = (currentTextID+1) % texts.length;
  }
   
  // timer
   if(startTime + 3000 > millis()) {
     displaySpeachBubble(50,50, currentTextID);
   } else {
     showMe = false;
   }
     
   if(mousePressed) saveFrame("face-###.png");
}


void displayBackground() {
  background(lightblue);
  
  /*** Cloud ***/
  wolkenPos = wolkenPos +0.1;
  if (wolkenPos == width) wolkenPos = -200;
  shape(wolke, wolkenPos+1, 50);
}


void displayBlankFace(float posX, float posY) {

  noStroke();

  /*** Shoulders ***/
  fill(blue);
  rect(posX+160, posY+400, 300, 200, 32, 32, 0, 0);

  /*** Face ***/
  fill(orange);
  rect(posX+200, posY+100, 200, 360, 3, 6, 32, 12);

  /*** Hat ***/
  fill(darkblue);
  quad(posX+140, posY+120, posX+220, posY+80, posX+440, posY+70, posX+420, posY+160);
}


void displayEye(float posX, float posY, float eyeSize, float pupilSize ) {

  /*** Eye ***/
  fill(255);

  float offset = eyeSize/2 - pupilSize/2;
  float tx = map(mouseX, 0, width, -offset, offset);
  float ty = map(mouseY, 0, height, -offset, offset);

  ellipse(posX, posY, eyeSize, eyeSize);
  fill(0);
  ellipse(posX+tx, posY+ty,pupilSize, pupilSize);
}

void displayMouth (float posX, float posY, float minScale, float maxScale) {
  
  /*** Mouth ***/
  getVolume(); // this call fetches the mic volume for the current frame.
  float scale = map(volume, 0,100, minScale, maxScale);  // now we can use it for something!

 // if(volume > 60 && showSpeach == false)  saySomething();

  fill(red);
  arc(posX, posY, 50, scale, radians(0), radians(180)); 
   
}

void displaySpeachBubble(float posX, float posY, int textID) {
    fill(250);
    rect(posX, posY, 250, 200, 25);   
    triangle(posX+225,posY+200,posX+250,posY+180,posX+280,posY+280);
    fill(0);
    textSize(24);
    text(texts[textID], posX+20, posY+50);  
}
// FACECATCH
// based on Ricard Marxer fisica example ContactRemove
// external libraries: fisica, gab.opencv, processing.video
// by Jonas Hansen, 24/10/16

import gab.opencv.*;
import processing.video.*;
import java.awt.*;
import fisica.*;

Capture video;
OpenCV opencv;

FWorld world;
FBox faceBox;

int score;

//PImage img;

void setup() {
  
  String[] cameras = Capture.list();
  
  for (int i = 0; i < cameras.length; i++) {
      println(cameras[i]);
    }
  
  size(640, 480);
  
  video = new Capture(this, 320, 240);
  opencv = new OpenCV(this, 320, 240);
  opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);  
  
  video.start();
  Fisica.init(this);
  world = new FWorld();
  
  // Face 
  faceBox = new FBox(100,50);
  faceBox.setPosition(width/2, height - 40);
  faceBox.setStatic(true);
  faceBox.setNoFill();
  faceBox.setStroke(255);
  faceBox.setSensor(true);
  //img = loadImage("avatar.png");
  //faceBox.attachImage(img);
  world.add(faceBox);


}

void draw() {
  scale(2);
  opencv.loadImage(video);
  image(video, 0, 0 );

  Rectangle[] faces = opencv.detect();
  //println(faces.length);

  // Draw Face Box
  for (int i = 0; i < faces.length; i++) {
    //println(faces[i].x + "," + faces[i].y);
    //rect(faces[i].x, faces[i].y, faces[i].width, faces[i].height);
    faceBox.setPosition(faces[i].x+faces[i].width/2, faces[i].y+ faces[i].height/2);
    faceBox.setWidth(faces[i].width);
    faceBox.setHeight(faces[i].height);
  }
  
  // Draw Score
  fill(255);
   textAlign(CENTER);    
   
  if(score > 0) {
      text(score, faceBox.getX(),faceBox.getY());
   }else {
     text("catch balls", faceBox.getX(),faceBox.getY());
   }
  
  
  
  // Draw Balls
  if (frameCount % 8 == 0) {
    FCircle b = new FCircle(random(5, 20));
    b.setPosition(random(0+10, width-10), -50);
    b.setVelocity(0, 200);
    b.setRestitution(0);
    b.setNoStroke();
    b.setFill(255);
    world.add(b);
  }
    
  world.draw();
  world.step();

}


void contactStarted(FContact c) {
  FBody ball = null;
  if (c.getBody1() == faceBox) {
    ball = c.getBody2();
  } else if (c.getBody2() == faceBox) {
    ball = c.getBody1();
  }
  
  if (ball == null) {
    return;
  }
  
  world.remove(ball);
  score = score +1;
}

void captureEvent(Capture c) {
  c.read();
}
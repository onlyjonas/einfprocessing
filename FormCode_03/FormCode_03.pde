/* * * Form & Code 03 * * */
/* * * Struktur 2 * * * * */

// Fallunterscheidungen - Entweder: if...
  /*
  if (Ja/Nein-Frage) {
    Anweisung1;
    Anweisung2;
    Anweisung3;
  }
  */

float zahl = random(100);

if (zahl < 50) {
  line(10,70,80,30);
}

// Fallunterscheidungen ...oder: else

if (zahl < 50) {
  ellipse(60,65,20,20);
}
else {
  rect(60,65,20,20);
}

/*
void draw() {
  background(255); 
  if(mouseX < width/2) {
    fill(0);
    rect(0,0,width/2,height);
  } else {
    ellipse(width/2 + 40, height/2, 80, 80);
  }
}
*/
void setup() {
  size(400,300);
  background(255);
}

void draw() {
  if (mousePressed) {
    vieleKreise(mouseX, mouseY);
  }
}

void vieleKreise(float woX, float woY) {
  noStroke();
  fill(0,128);
  for (int i=0; i<10; i++) {
    ellipse(woX+random(-20,20), woY+random(-20,20), 10, 10);
  }
}
float px;
float py;
float targetX;
float targetY;
float speed;

void setup() {
  size(800, 600);
  px = width/2;
  py = width/2;
  targetX = px;
  targetY = py;
  speed = 0.05;
}

void draw() {
  background(255);
  strokeWeight(128);
  point(px, py);
  if(mousePressed) {
    targetX = mouseX;
    targetY = mouseY;
  }
  px = px + (( targetX - px) * speed);
  py = py + (( targetY - py) * speed);  
}
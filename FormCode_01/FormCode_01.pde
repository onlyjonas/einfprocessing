/* * * Form & Code 01 * * */
/* * * Grundformen  * * * */


/* Setup */
size(600,600);
background(255);

/* Punkt */
stroke(0,0,255); // Linienfarbe: Blau 
point(50,50); 

/* Linie */
stroke(155); // Linienfarbe: Grau
line(110,10,90,height-10);

/* Rechteck */
stroke(0); // Linienfarbe: Schwarz
strokeWeight(5); // Strichstärke: 5px
fill(0,255,0); // Flächenfarbe: Grün
rect(150,50,100,100);

/* Kreis */
noStroke();
fill(0,0,255,80); // Flächenfarbe: Blau / Alpha: 60%
ellipse(250,150,100,100);

/* Dreieck & Viereck */
fill(255,0,0);
triangle(200,400,300,400,250,500);
quad(400,300,500,300,450,500,350,500);

/* Kreisbogen */
stroke(0);
strokeWeight(8);
noFill();
strokeCap(ROUND);
arc(width/2,height/2,40,40,0,radians(45));
strokeCap(SQUARE);
arc(width/2,height/2,60,60,radians(45),radians(120));
strokeCap(ROUND);
arc(width/2,height/2,90,90,radians(120),radians(340));

/* Komplexe Formen: shape */
fill(0,80);
strokeWeight(1);
beginShape();
vertex(40, 40);
vertex(width/2, 180);
vertex(360, 160);
vertex(120, 360);
vertex(40, 40);
endShape();
float px;
float py;

void setup() {
  size(800,600);
  px = width/2;
  py = height/2;
}

void draw() {
  background(255);
  strokeWeight(32);
  point(px,py);
  px = px + random(-1,1);
  py = py + random(-1,1);
}
float size = 200;
PVector position = new PVector (300, 300);

void setup() {
  size(600, 600);
}

void draw() {
  background(0); 

  float dist = dist(mouseX, mouseY, position.x, position.y);

  if (dist < size/2) {
    fill(255, 0, 0);
  } else {
    fill(255);
  }

  ellipse(position.x, position.y, size, size);
}
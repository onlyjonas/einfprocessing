void setup() {
  
 size(500,500);
 background(0);

 int dist = 50;
 fill(255);
 
 for(int i=1; i<10; i++){
   for(int j=1; j<10; j++){
     ellipse(dist*i, dist*j, 3*i, 3*i);  
   }
 }
 
}
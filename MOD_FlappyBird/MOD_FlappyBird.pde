// MODDING FLAPPYBIRD
// based on Daniel Shiffman
// Code for: https://www.youtube.com/watch?v=cXgA1d_E-jY
// external library: ddf.minim
// modified by Jonas Hansen, 24/10/16

Bird bird;
ArrayList<Pipe> pipes = new ArrayList<Pipe>();
AudioHandler audio;

boolean audioActive = true; // de-/activate audio controle
float minVol = 50.0;

int wid = 400;
int rez = 20;

int score = 0;
boolean jumping = false;

void setup() {
  size(400, 800);
  bird = new Bird();
  //pixelDensity(2);
  pipes.add(new Pipe());
  audio = new AudioHandler();
}

void draw() {
  background(0);

  float vol=0;
  if(audioActive) vol= audio.getVolume();

  if (frameCount % 75 == 0) {
    pipes.add(new Pipe());
  }

  if ((keyPressed) || (vol > minVol)) {
    PVector up = new PVector(0, -5);
    bird.applyForce(up);
  }
    

  bird.update();
  bird.show();

  boolean safe = true;

  for (int i = pipes.size()-1; i>=0; i--) {
    Pipe p = pipes.get(i);
    p.update();
    if (p.hits(bird)) {
      p.show(true);
      safe = false;
    } else {
      p.show(false);
    }

    if (p.x < -p.w) {
      pipes.remove(i);
    }
  }
  
  if (safe) {
    score++; 
  } else {
    score -= 50;
  }
  
  fill(255, 0, 255);
  textSize(64);
  text(score, width/2,50);
  
  score = constrain(score, 0, score);
  
  
}
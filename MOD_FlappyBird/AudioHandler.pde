import ddf.minim.*;

class AudioHandler {
  Minim minim;
  AudioInput in;
  float volume = 0;
  float volumeF = 0; 

  AudioHandler() {
    minim = new Minim(this);
    in = minim.getLineIn(Minim.MONO, 512);
  }

  float getVolume() {
    volumeF = in.right.level()*1000;
    volume = 0.8*volume + 0.2*volumeF;
    return volume;
  }

  void stop() {
    in.close();
    minim.stop();
    this.stop();
  }
}
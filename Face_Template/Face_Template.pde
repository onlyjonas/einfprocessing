/**
 *    Titel: Face Template
 *    Name :
 *    Datum: 
 */
  
/* Variablen */
color lightblue;
PShape wolke;
float wolkenPos;

/* Setup */
void setup() {
  
  // Variablen
  lightblue = color(0, 221, 255);
  wolke = loadShape("Wolke.svg");

  // Größe
  size(800, 600);
  
  // Activate AudioHandler
  //setupAudio();

}

/* Update Loop */
void draw() {
  displayBackground();
  // Gesicht (Variablen müssen gesetzt werden)
  displayBlankFace(0, 0);   
  displayEye(0, 0, 0, 0);
  displayEye(0, 0, 0, 0);
  displayMouth(0, 0, 0, 0);
}

/* Funktionen */
void displayBackground() {
  
  background(lightblue);
  
  // Wolke
  wolkenPos = wolkenPos +0.1*10;
  if (wolkenPos == width) wolkenPos = -200;
  shape(wolke, wolkenPos, 50);

}


void displayBlankFace(float posX, float posY) {

  // Schultern

  // Gesicht
  
  // Haare ...
  
}


void displayEye(float posX, float posY, float eyeSize, float pupilSize ) {

}

void displayMouth (float posX, float posY, float minScale, float maxScale) {

}
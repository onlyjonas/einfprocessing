import gab.opencv.*;
import processing.video.*;
import java.awt.*;
import fisica.*;
import megamu.mesh.*;

// TODO create HULL broken ...

Capture video;
OpenCV opencv;

FPoly myPoly;
FWorld world;

void setup() {

  size(640, 480);
  video = new Capture(this, 640, 480);
  opencv = new OpenCV(this, 640, 480);
  video.start();
  opencv.startBackgroundSubtraction(5, 3, 0.5);

  Fisica.init(this);

  world = new FWorld();
}

void draw() {

  opencv.loadImage(video);
  image(video, 0, 0);  

  opencv.updateBackground();
  opencv.dilate();
  opencv.erode();

  noFill();
  ArrayList<PVector> points = new ArrayList<PVector>();

  for (Contour contour : opencv.findContours()) {
    stroke(255, 0, 0);
    contour.draw();
    for (PVector point : contour.getPolygonApproximation().getPoints()) {
      points.add(point);
    }
  }
  float p[][] = new float[points.size()][2];

  for (int i = 0; i < points.size(); i++) {
    p[i][0] = points.get(i).x;
    p[i][0] = points.get(i).y;
  }

  // redraw poly Hull
  world.remove(myPoly);
  myPoly= new FPoly();
  myPoly.setStatic(true);
  Hull myHull = new Hull(p);

  int[] extremPoints = myHull.getExtrema();
  for (int j=0; j<extremPoints.length; j++) {
    myPoly.vertex(p[extremPoints[j]][0], p[extremPoints[j]][1]);
  }
  
  world.step();
  world.draw();
}

void captureEvent(Capture c) {
  c.read();
}
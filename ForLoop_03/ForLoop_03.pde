void setup() {

  size(500, 500);
  background(0);

  int dist = 100;
  fill(255);

  for (int i=1; i<5; i++) {
    for (int j=1; j<5; j++) {
      drawCircle(dist*i, dist*j, 80, 10);
    }
  }
}

void drawCircle(float x, float y, float scale, int amount) {

  color red = color(255, 0, 0); 
  color white = color(255);
  color col = white;

  for (int i=0; i<amount+1; i++) {
    if (col == white) col = red;
    else col = white;
    fill(col);
    noStroke();
    ellipse(x, y, scale-i*(scale/amount), scale-i*(scale/amount));
  }
}
// based on Daniel Shiffman
// http://codingrainbow.com
// http://patreon.com/codingrainbow
// Code for: https://youtu.be/AaGK-fj-BAM
import ddf.minim.*;
import ddf.minim.ugens.*;

Minim minim;
AudioOutput out;

Snake snake;
Food[] food;
int amountFood = 1;
int scale = 20;
int scene = 0;

void setup() {
  size(400, 400);
  
  snake = new Snake(0,0);
  
  food = new Food[amountFood];
  for(int i=0; i<food.length; i++) {
    food[i] = new Food();  
  }
  
  frameRate(10);
  textSize(32);
  noCursor();
  
  // audio
  minim = new Minim(this);
  out = minim.getLineOut();
  //out.setTempo( 80 );
}

void draw() {
  background(51);

  switch(scene) {

  case 0: // start scene
    
    textAlign(CENTER);
    text("S N A K E\npress any key to start", width/2, height/2-20);

    if (keyPressed) scene++;
    break;

  case 1: // game scene
    
    for( Food f : food) {
      if (snake.eat(f.position)) f.pickLocation();
      f.show();
    }
    if (snake.death()) scene ++;
    snake.update();
    snake.show();

    break;

  case 2: // game over scene
    
    textAlign(CENTER);
    fill(255);
    text("GAME OVER\nscore: "+snake.score, width/2, height/2-20);
    if (keyPressed) scene = 0;
    break;
    
  }
}


void keyPressed() {
  if (keyCode == UP) {
    snake.dir(0, -1);
  } else if (keyCode == DOWN) {
    snake.dir(0, 1);
  } else if (keyCode == RIGHT) {
    snake.dir(1, 0);
  } else if (keyCode == LEFT) {
    snake.dir(-1, 0);
  } 
}
// Daniel Shiffman
// http://codingrainbow.com
// http://patreon.com/codingrainbow
// Code for: https://youtu.be/AaGK-fj-BAM

class Snake {
  float x = 0;
  float y = 0;
  float xspeed = 1;
  float yspeed = 0;
  int total = 0;
  int score = 0;
  
  ArrayList<PVector> tail = new ArrayList<PVector>();

  Snake(float xPos, float yPos) {
    x = xPos;
    y = yPos;
  }

  boolean eat(PVector pos) {
    float d = dist(x, y, pos.x, pos.y);
    if (d < 1) {
      total++;
      out.playNote( 60+20*total );
      return true;
    } else {
      return false;
    }
  }

  void dir(float x, float y) {
    xspeed = x;
    yspeed = y;
  }

  boolean death() {
    boolean dead = false;
    for (int i = 0; i < tail.size(); i++) {
      PVector pos = tail.get(i);
      float d = dist(x, y, pos.x, pos.y);

      if (d < 1) {
        println("starting over");
        score = total;
        total = 0;
        tail.clear();
        dead = true;
        for (int j=0; j<score; j++) out.playNote(0.2*j, 60+20*score-j );
      }
    }
    return dead;
  }

  void update() {
    
    //println(total + " " + tail.size());
    if (total > 0) {
      if (total == tail.size() && !tail.isEmpty()) {
        tail.remove(0);
      }
      tail.add(new PVector(x, y));
    }

    x = x + xspeed*scale;
    y = y + yspeed*scale;

    x = constrain(x, 0, width-scale);
    y = constrain(y, 0, height-scale);
  }

  void show() {
    fill(255);
    for (PVector v : tail) {
      rect(v.x, v.y, scale, scale);
    }
    rect(x, y, scale, scale);
  }
}
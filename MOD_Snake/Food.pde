
class Food {
  PVector position;
  color col;
  int points = 1;
  
  ArrayList<PVector> tail = new ArrayList<PVector>();

  Food() {
    col = color(random(255), random(255), random(255));
    pickLocation();
  }
 
  
  void pickLocation() {
    int cols = width/scale;
    int rows = height/scale;
    position = new PVector(floor(random(cols)), floor(random(rows)));
    position.mult(scale);
  }
  
  void show() {
    fill(col);
    rect(position.x, position.y, scale, scale); 
  }
  
}
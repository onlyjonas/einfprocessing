void setup() {
  
 size(500,500);
 background(0);

 int startX=0;
 int startY=0;
 int endX=0;
 int endY=0;
 stroke(255);
 
 for(int i=1; i<10; i++){
   
   startX = 50*i;
   startY=0;
   endX=500;
   endY=50*i;  
   
   line(startX,startY,endX,endY);
 }
 
}
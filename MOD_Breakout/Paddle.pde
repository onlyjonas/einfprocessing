// PADDLE

public class Paddle {
  
  Rectangle rectangle;
  // PADDLE PROPERTIES --
  int myWidth = 60;
  int myHeight = 10;
  float speed = 5.0;
  boolean hasStroke = false;
  color strokeColor = #FFFFFF;
  boolean hasFill = true;
  color fillColor = #ffffff;
  //
  int x = gameFrameWidth/2;
  int y = gameFrameHeight-50;
  //
  //
  
  
  
  Paddle() {
    rectangle = new Rectangle(myWidth, myHeight, hasStroke, strokeColor, hasFill, fillColor);
    rectangle.setPosition(x, y);
  }
  
  
  
  void refresh(){
    updatePosition();
    rectangle.setPosition(x, y);
    rectangle.drawYourself();
  }
  

  void updatePosition() {
    //x = mouseX-recX-myWidth/2;
    if(keyPressed) {
     if(key == CODED && keyCode == LEFT) x-= speed;
     else if(key == CODED && keyCode == RIGHT)x+= speed;
    }
    x = constrain(x, 0, gameFrameWidth-myWidth);
  }

}
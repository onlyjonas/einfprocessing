/**
 * oscP5sendreceive by andreas schlegel
 * example shows how to send and receive osc messages.
 * oscP5 website at http://www.sojamo.de/oscP5
 */

import oscP5.*;
import netP5.*;

OscP5 oscP5;


PVector position;
float size = 50;
boolean colorToggle;

void setup() {
  size(400, 400);
  frameRate(25);
  /* start oscP5, listening for incoming messages at port 12000 */
  oscP5 = new OscP5(this, 8000);

  // rect
  fill(255);
  rectMode(CENTER);
  position = new PVector(width/2, 300);
}


void draw() {
  background(0); 

  if (colorToggle) fill(random(255), random(255), random(255));
  rect(position.x, position.y, size, size);
}


/* incoming osc message are forwarded to the oscEvent method. */
void oscEvent(OscMessage theOscMessage) {
  /* print the address pattern and the typetag of the received OscMessage */
  print("### received an osc message.");
  print(" addrpattern: "+theOscMessage.addrPattern());
  println(" typetag: "+theOscMessage.typetag());

  if (theOscMessage.checkAddrPattern("button1")==true) changeSize(10);
  if (theOscMessage.checkAddrPattern("button")==true) changeSize(-10);

  if (theOscMessage.checkAddrPattern("/hslider")==true) {
    float hSlider = 0;
    if (theOscMessage.checkTypetag("s") ) {
      String s = theOscMessage.get(0).stringValue();
      s = s.replaceAll(",", ".");
      hSlider = float(s);
    }
    if (theOscMessage.checkTypetag("f") ) {
      hSlider = theOscMessage.get(0).floatValue();
    }
    updatePos(map(hSlider, 0, 1, 0, width));
  }


  if (theOscMessage.checkAddrPattern("/toggle")==true) {
    if (theOscMessage.checkTypetag("i") ) {
      int i = theOscMessage.get(0).intValue();
      colorToggle = boolean(i);
    }
  }
}

void updatePos(float pos) {
  position.x = pos;
}

void changeSize(float factor) {
  size = size + factor;
  constrain(size, 10, width);
}
// MakeyMakey Instrument
// simple instrument to test makey makey input
// board: http://makeymakey.com/how-to/classic/
// by Jonas Hansen, 26/10/16

import ddf.minim.*;
AudioOutput out;
boolean active;
float size = 150;

void setup()
{
  fullScreen();
  Minim minim = new Minim( this );
  out = minim.getLineOut();
}

void draw() 
{
  background(0);

  if (keyPressed) {

    if (keyCode == UP) {
      if (!active) out.playNote(0, 0.2, "C4");
      active = true;
      ellipse(width/2, height/2 - size, size, size);
    }

    if (keyCode == DOWN) {
      if (!active) out.playNote(0, 0.2, "E3");
      active = true;
      ellipse(width/2, height/2 + size, size, size);
    }

    if (keyCode == LEFT) {
      if (!active) out.playNote(0, 0.2, "A3");
      active = true;
      ellipse(width/2 - size, height/2, size, size);
    }

    if (keyCode == RIGHT) {
      if (!active) out.playNote(0, 0.2, "B3");
      active = true;
      ellipse(width/2 + size, height/2, size, size);
    }

    if (key == ' ') {
      if (!active) out.playNote(0, 0.2, "D3");
      active = true;
      rect(0, 0, width/2, height);
    }
  }

  if (mousePressed == true) {
    if (!active) out.playNote(0, 0.2, "C3");
    active = true;
    rect(width/2, 0, width/2, height);
  }
}

void keyReleased() {
  active = false;
}

void mouseReleased() {
  active = false;
}
/* * * Face 01 * * */

color blue = color(34, 92, 204);
color darkblue = color(65, 95, 153);
color lightblue = color(0, 221, 255);
color orange = color(255, 135, 85);
color red = color(204, 31, 0);

size(600, 600);

background(lightblue);
noStroke();

/*** Shoulders ***/
fill(blue);
rect(160, 400, 300, 200, 32, 32, 0, 0);

/*** Face ***/
fill(orange);
rect(200, 100, 200, 360, 3, 6, 32, 12);

/*** Hat ***/
fill(darkblue);
quad(140, 120, 220, 80, 440, 70, 420, 160);  

/*** Eyes ***/
fill(255);
ellipse(240, 260, 20, 20);
ellipse(280, 260, 40, 40);
fill(0);
ellipse(240, 260, 10, 10);
ellipse(280, 260, 20, 20);

/*** Mouth ***/
fill(red);
arc(260, 320, 50, 100, radians(0), radians(180));

//save("face.png");
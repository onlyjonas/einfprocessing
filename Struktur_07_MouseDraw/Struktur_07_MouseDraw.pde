void setup() {
  size(400,300);
  frameRate(25);
  background(255);
}

void draw() {
  stroke(0,128);
  strokeWeight(12);
  point(mouseX,mouseY);
}
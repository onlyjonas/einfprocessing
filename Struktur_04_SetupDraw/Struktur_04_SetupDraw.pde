void setup() {
  size(400,300);
  frameRate(25);
  background(255);
}

void draw() {
  strokeWeight(1);
  stroke(0,128);
  line(width/2, height/2, random(0,width), random(0,height));
}
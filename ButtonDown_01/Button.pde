class Button {

  PVector position;
  color defaultCol;
  color overCol;
  color activeCol;
  float size;
  boolean over;
  boolean active;
  
  Button (float myX, float myY, float mySize, color myDefaultCol, color myOverCol, color myActiveCol) {
    position = new PVector(myX, myY);
    size = mySize;
    defaultCol = myDefaultCol;
    overCol = myOverCol;
    activeCol = myActiveCol;
    
    over = false;
    active = false;

  }

  void update () {
    // mouse is over button
    
    float dist = dist(mouseX, mouseY, position.x, position.y);
    
    
    // check if mouse is over
    if (dist< size/2) {
      over = true;  
    } else {
      over = false; 
    }
    
    // check if mouse is pressed
    if (mousePressed == true && over == true) {
      active = true;
    } else {
      active = false;
    }  
    
  }

  void display () {
    if (active) fill(activeCol);
    else if(over) fill(overCol);
    else fill(defaultCol);
    noStroke();
    ellipse(position.x, position.y, size, size);
  }
}
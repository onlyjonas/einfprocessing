/* * * Button Down * * */

Button[] myButtons;

void setup() {

  //fullScreen();
  size (800,800);
  myButtons = new Button[100];
  for(int i = 0; i<myButtons.length-1; i++) {
    myButtons[i] = new Button(random(25, width-25), random(25, height-25), random(10,100), color(255), color(200), color(255,0,0));
  }
}

void draw () {
  
  background(0);
  
  for(int i = 0; i<myButtons.length-1; i++) {
  myButtons[i].update();
  myButtons[i].display();
  }

}
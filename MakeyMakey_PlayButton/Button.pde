
class Button {

  int myId;
  PVector position;
  float scale;
  color defaultCol;
  color activeCol;
  color pressCol;
  float triggerTime;
  boolean active = false;
  boolean pressed = false;

  Button(int id, PVector pos, float scl, color dCol, color aCol, color pCol) {
    myId = id;
    position = pos;
    scale = scl;
    defaultCol = dCol;
    activeCol = aCol;
    pressCol = pCol;
  }

  void update () {
  }

  void display () {
    if (active) fill(activeCol); 
    else fill(defaultCol);

    if (pressed) fill(pressCol);
    ellipse(position.x, position.y, scale, scale);
  }

  void activate () {
    active = true;
  }

  void deactivate() {
    active = false;
  }

  void press(int id) {
    if (active) {
      if (id == myId && pressed == false) {
    
        pressed = true; 
      }
    }
  }

}
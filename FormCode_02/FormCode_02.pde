/* * * Form & Code 02 * * */
/* * * Struktur 1 * * * * */

size(200,200);
noStroke();
background(255);


/* Zufall */
fill(random(0,200));
rect(random(10,width-10), random(10,height-10), 20, 20);

/* Debug */
println(random(100));
println(" ist eine Zufallszahl.");

print(random(100));
println(" ist eine Zufallszahl.");

/* Variablen */
// initialisieren
int px;
float py;

// Werte zuweisen
px = 30;
py = 35.0;

// benutzen:
point(px,py);
ellipse(px+30,py+30,20,20);

// Rechnen plus +, minus -, mal *, geteilt /
float breite = width/2;
float hoehe = height/2;

noFill();
stroke(1);
rectMode(CENTER);
rect(breite,hoehe,width-breite*0.5,height-hoehe*0.5);

// Achtung: beim Rechnen mit float und int wird alles float.
int i = 10;
float f = 10;
println("i: "+i+" f: "+f+" i+f="+(i+f));
// Beim Rechnen mit int wird abgerundet.
int zahl = 10;
int zweiteZahl = 3;
println(zahl/zweiteZahl);


// Variablen müssen nicht zwangsläufig reine Zahlen sein. Beispiel: Farbe
color orange = color(255,100,0);
color randomColor = color(random(255),random(255),random(255));
stroke(orange);
fill(randomColor);
rect(random(20,width-20),random(height-20),40,40);
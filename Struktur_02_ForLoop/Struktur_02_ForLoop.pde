/* For Loop */

size(200,200);
background(255);
noStroke();
fill(0);
rectMode(CENTER);

// Eine Zeile
for (int i=0; i<10; i++) {
  rect(10+i*20,10,8,8); 
}

// Eine Spalte
//for (int j=0; j<10; j++) {
//  rect(10,10+j*20,8,8); 
//}

// Zeilen und Spalten
//for (int i=0; i<10; i++) {
//  for (int j=0; j<10; j++) {
//  rect(10+i*20,10+j*20,8,8); 
//  }
//}

// ... mit zusätzlicher Variable.
//for (int i=0; i<10; i++) {
//  for (int j=0; j<10; j++) {
//  fill(i/10.0*255,0,j/10.0*255);
//  float groesse = random(1,9);
//  rect(10+i*20,10+j*20,groesse,groesse); 
//  }
//}
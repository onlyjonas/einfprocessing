ArrayList<Button> buttons;

void setup() {
  size(600, 600);

  buttons = new ArrayList();
}

void draw() {
  background(0); 

  for (int i=0; i<buttons.size(); i++) {
    Button nextButton = buttons.get(i);
    nextButton.update();
    nextButton.display();
  }
}

void mousePressed() {
  if (mouseButton == LEFT) {
    color newColor = color(random(50, 200), random(50, 200), random(50, 200), 200);
    buttons.add(new Button(mouseX, mouseY, random(20, 100), newColor, newColor + color(55)));
  } else if (mouseButton == RIGHT) {
    for (int i=0; i<buttons.size(); i++) {
      Button b = buttons.get(i);
      if (b.over) buttons.remove(i);
    }
  }
}
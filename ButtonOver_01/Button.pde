
class Button {

  PVector position;
  color defaultCol;
  color overCol;
  float size;
  
  boolean over;

  Button (float myX, float myY, float mySize, color myDefaultCol, color myOverCol) {
    position = new PVector(myX, myY);
    size = mySize;
    defaultCol = myDefaultCol;
    overCol = myOverCol;
  }

  void update () {

    float dist = dist(mouseX, mouseY, position.x, position.y);

    // check mouse over
    if (dist < size/2) {
      over = true;
    } else {
      over = false;
    }

  }

  void display () {
    if (over)  fill(overCol);
    else fill(defaultCol);
    ellipse(position.x, position.y, size, size);
  }

}
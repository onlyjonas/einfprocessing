void setup() {
  size(400,300);
  background(255);
}

void draw() {
  if (mousePressed) {
    vieleKreise();
  }
}

void vieleKreise() {
  noStroke();
  fill(0,128);
  for (int i=0; i<10; i++) {
    ellipse(random(0,width), random(0,height), 10, 10);
  }
}
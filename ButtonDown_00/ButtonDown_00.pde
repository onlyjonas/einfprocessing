/* * * Button Down * * */

Button myButton;

void setup() {

  fullScreen();
  myButton = new Button(width/2, height/2, height/5, color(255), color(200), color(255,0,0));
  
}

void draw () {
  
  background(0);
  myButton.update();
  myButton.display();

}